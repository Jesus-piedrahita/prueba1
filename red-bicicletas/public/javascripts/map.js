
//ubicacion principal - Cali
let map = L.map('main_map').setView([3.395397,-76.6657533],11);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/*L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiamVzdXMtcGllZHJhaGl0YSIsImEiOiJja2dsb3RrOW8wMW8xMnNtbG1ic3BwOGVvIn0.PqhGGnflhzWKpr7kgAM2zQ'
}).addTo(map);*/

L.marker([3.2281773, -76.5135678]).addTo(map);//bonanza
L.marker([3.4070071, -76.5007027]).addTo(map);//vallado
L.marker([3.4196084, -76.5495586]).addTo(map);//tequendama
L.marker([3.4224331, -76.561583]).addTo(map);//siloe


$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: function(result){
        console.log('resultado del console: '+result)
        result.bicicleta.forEach(function(element){
            L.marker(element.ubicacion, {title: element.id}).addTo(map)
        });
    }
})