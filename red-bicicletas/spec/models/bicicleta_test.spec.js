let bicicleta = require('../../Models/bicicleta');

beforeEach(() => {
    bicicleta.allBicis = [];
})

describe('bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(bicicleta.allBicis.length).toBe(0)
    });
});

describe('bicicleta.add', () => {
    it('agregamos una', () => {
        expect(bicicleta.allBicis.length).toBe(0);

        let a = new bicicleta(0, 'roja', 'urbana', [3.2281773, -76.5135678]);
        bicicleta.add(a);

        expect(bicicleta.allBicis.length).toBe(1);
        expect(bicicleta.allBicis[0]).toBe(a);
    })
})

describe('bicicleta.findById', () => {
    it('debe devolver la bicicleta con id en 1', () => {
        expect(bicicleta.allBicis.length).toBe(0);
        let aBicis2 = new bicicleta(1, 'verde', 'rural');
        let aBicis3 = new bicicleta(2, 'marron', 'urbano');
        bicicleta.add(aBicis2);
        bicicleta.add(aBicis3);

        let targetBicis = bicicleta.findById(1);
        expect(targetBicis.id).toBe(1);
        expect(targetBicis.color).toBe(aBicis2.color);
        expect(targetBicis.modelo).toBe(aBicis2.modelo);
    })
})