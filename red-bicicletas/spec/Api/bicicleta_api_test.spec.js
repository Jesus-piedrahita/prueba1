let bicicleta = require('../../Models/bicicleta');
let req = require('request');
//let server = require('../../bin/www');
describe('testiando...', () => {
    describe('bicicletas Api', () => {
        describe('get bicicletas', () => {
            console.log('testiando por medio de consola')
            it('status 200', () => {
                expect(bicicleta.allBicis.length).toBe(0);
    
                let a = new bicicleta(1, 'roja', 'urbana', [3.2281773, -76.5135678]);
                bicicleta.add(a);
    
                req.get('http://localhost:3000/api/bicicleta', function(error, response, body){
                    expect(response.statusCode).toBe(200);
                })
            })
        })
    
        describe('post bicicleta/create', () => {
            it('status 200', (done) => {
                let headers = {'content-type' : 'application/json'};
                let aBicis = '{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -14, "lon": -54}';
                req.post({
                    headers: headers,
                    url: 'http://localhost:3000/api/bicicletas/create',
                    body: aBicis
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(bicicleta.findById(10).color).toBe(rojo);
                    done();
                })
    
            })
        })
    })
        
})
