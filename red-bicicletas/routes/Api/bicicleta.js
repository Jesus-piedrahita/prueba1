let express = require('express')
let router = express.Router()
let bicicletaControllerApi = require('../../Controllers/Api/bicicletaControllerApi')

router.get('/', bicicletaControllerApi.bicicleta_list)
router.post('/create', bicicletaControllerApi.bicicleta_create)
router.delete('/delete', bicicletaControllerApi.bicicleta_delete)

module.exports = router