let mongoose = require('mongoose');
let schema = mongoose.Schema;

let bicicletaSchema = new schema({
    code : Number,
    color : String,
    modelo : String,
    ubicacion : {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
})

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color, 
        modelo: modelo,
        ubicacion: ubicacion
    })
}

bicicletaSchema.method.toString = function(){
    return `code: ${this.code} | color: ${this.color}`
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb)
}

let bicicleta  = function(id, color, modelo, ubicacion){
    this.id = id
    this.color = color
    this.modelo = modelo
    this.ubicacion = ubicacion
}

bicicleta.prototype.toString = function(){
    return `Id: ${this.id} | Color: ${this.color}`
}

bicicleta.allBicis = []
bicicleta.add = function(aBicis){
    bicicleta.allBicis.push(aBicis)
}

let b, v, t, s

bicicleta.findById = function(aBici){
    let bici = bicicleta.allBicis.find(x => x.id == aBici)
    if(bici)
        return  bici
    else
        throw new Error('La bicicleta no existe')

}

bicicleta.removeById = function(aBici){
    for(let i = 0; i < bicicleta.allBicis.length; i++){
        if(bicicleta.allBicis[i].id == aBici){
            bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

/*b = new bicicleta(1, 'roja', 'urbana', [3.2281773, -76.5135678])
v = new bicicleta(2, 'verde', 'rural', [3.4070071, -76.5007027])
t = new bicicleta(3, 'rosada', 'rural', [3.4196084, -76.5495586])
s = new bicicleta(4, 'blanca', 'urbana', [3.4224331, -76.561583])

bicicleta.add(b)
bicicleta.add(v)
bicicleta.add(t)
bicicleta.add(s)*/

module.exports = bicicleta